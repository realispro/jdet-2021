package lab.zoo;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import lab.zoo.animals.*;

import java.util.*;

public class ZooMain {


    public static void main(String[] args) {

        Bird kiwi = new Kiwi("Janek", 5);
        Bird eagle = new Eagle("Bielik", 15);

        Collection<Animal> animals = new TreeSet<>(new AnimalComparator().reversed());

        animals.add(kiwi);
        animals.add(new Shark("Joe", 300));
        animals.add(eagle);

        Collection<Bird> birds = new ArrayList<>();
        birds.add(kiwi);
        birds.add(eagle);

        printInventory(animals);

        Collection<Fish> fishes = new ArrayList<>();
        fishes.add(new Shark("Joe", 300));

        addFish(fishes);


        System.out.println("Bielik included? " + animals.contains(new Eagle("Bielik", 15)));

        BiMap<Animal, String> careTakers = HashBiMap.create();
        careTakers.put(new Eagle("Bielik", 15), "Kazik");
        careTakers.put(kiwi, "Jozek");
        //careTakers.put(kiwi, "Kazik");

        String careTaker = careTakers.get(new Kiwi("Janek", 5));
        System.out.println("careTaker = " + careTaker);

        BiMap<String, Animal> inversed = careTakers.inverse();
        System.out.println("Kazik takes care of " + inversed.get("Kazik"));

        for ( Map.Entry<Animal, String> entry : careTakers.entrySet()){
            System.out.println("entry key: " + entry.getKey() + ", value=" + entry.getValue());
        }

        AnimalPair<String> pair = new AnimalPair<>(eagle, "Zenek");
        Animal animalFromPair = pair.getFirst();
        String zenek = pair.getSecond();
        System.out.println("from pair: first=" + animalFromPair + ", second=" + zenek);

    }

    // PECS - Producer extends, Consumer super
    public static void printInventory(Collection<? extends Animal> animals){
        for(Animal animal : animals){
            System.out.println("animal = " + animal);
        }
        // WRONG animals.add(new Shark("Joe", 200));
    }

    public static void addFish(Collection<? super Shark> animals){
        animals.add(new Shark("George", 234));
    }


}
