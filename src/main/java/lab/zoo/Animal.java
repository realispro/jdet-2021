package lab.zoo;

import java.util.Objects;

public abstract class Animal {

    protected String name;

    protected int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public void eat(String food){
        System.out.println(name + " is consuming " + food);
    }

    public abstract void move();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return size == animal.size &&
                Objects.equals(name, animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, size);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
