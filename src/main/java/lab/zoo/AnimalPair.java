package lab.zoo;

import lab.common.Pair;

public class AnimalPair<T> extends Pair<Animal, T> {

    public AnimalPair(Animal first, T second) {
        super(first, second);
    }
}
