package lab.zoo.animals;

import lab.zoo.Animal;

public abstract class Fish extends Animal {

    public Fish(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println(name + " is swimming...");
    }
}
