package lab.zoo.animals;

public final class Eagle extends Bird{

    public Eagle(String name, int size) {
        super(name, size);
    }
}
