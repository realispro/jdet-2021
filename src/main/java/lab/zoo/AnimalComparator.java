package lab.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal>{
    @Override
    public int compare(Animal a1, Animal a2) {
        return a1.name.compareTo(a2.name);
    }
}
