package lab.devs;


import lab.common.Person;

import java.util.HashSet;
import java.util.Set;

public final class Developer extends Person {

    private Set<Skill> skills = new HashSet<>();

    public Developer(String firstName, String lastName){
        super(firstName, lastName);
    }

    public void addSkill(Skill skill) throws IllegalStateException{
        skills.add(skill);
   }

    public Set<Skill> getSkills() {
        return skills;
    }


    @Override
    public String toString() {
        return "Developer{" +
                "skills=" + skills +
                '}';
    }
}
